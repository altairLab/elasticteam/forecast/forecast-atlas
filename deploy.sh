# Script to deploy the app to Pypi

function upload_deploy() {
    twine upload dist/*
}

function upload_test() {
    twine upload --repository-url https://test.pypi.org/legacy/ dist/*
}

make build
pip3 install --upgrade setuptools twine wheel
python3 setup.py sdist bdist_wheel

PS3="Please enter your choice: "
options=("Upload to Test-Pypi" "Deploy to Pypi" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Upload to Test-Pypi")
            upload_test
            break;
            ;;
        "Deploy to Pypi")
            upload_deploy
            break;
            ;;
        "Quit")
            exit 1
            ;;
        *) echo "Invalid option $REPLY";;
    esac
done
