import Vue from 'vue'
import App from './App.vue'
import axios from "./plugins/axios"
import store from "./plugins/store"
import socket from "./plugins/socketio"
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import VueNotification from '@mathieustan/vue-notification';
import VueEllipseProgress from 'vue-ellipse-progress';

Vue.use(VueEllipseProgress);

Vue.use(VueNotification, {
  theme: {
    colors: {
      success: '#4f88ff',
      successDarken: '#2d71fe',
      info: '#5d6a89',
      infoDarken: '#535f7b',
      warning: '#f8a623',
      warningDarken: '#f69a07',
      error: '#ff4577',
      errorDarken: '#ff245f',
      offline: '#ff4577',
      offlineDarken: '#ff245f',
    },
    boxShadow: `0px 3px 5px -1px rgba(0,0,0,0.2),
      0px 6px 10px 0px rgba(0,0,0,0.14),
      0px 1px 18px 0px rgba(0,0,0,0.12)`,
  },
  breakpoints: {
    0: {
      bottom: true,
    },
    480: {
      right: true,
    },
  },
});

Vue.config.productionTip = false
Vue.prototype.$axios = axios
Vue.prototype.$socket = socket

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
