import Vuex from 'vuex'
import Vue from 'vue'

import drawer from "./store/drawer"
import logs from "./store/logs"
import coms from "./store/coms"
import status from "./store/status"
import settings from "./store/settings"
import connection from "./store/connection"

Vue.use(Vuex)


export default new Vuex.Store({
    state: {

    },

    getters: {

    },

    mutations: {

    },

    actions: {

    },
    modules: {
        drawer,
        logs,
        coms,
        status,
        settings,
        connection
    }
  });