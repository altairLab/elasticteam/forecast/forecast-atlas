import Vue from 'vue';
export const EventBus = new Vue();

/*

Main Event Bus of the application.
Since the use cycle of the application is quite straightforward,
an event bus comes as an easy to use solution to resetting/notifying the state
of most widgets.

Events:

- resetWidgets  -> When the client connects to the board, all widgets must reset to a neutral state
- logStarted -> The first log is displayed
- disconnect -> A component asked to disconnect (e.g. the timer)
- disconnected -> User presses the STOP button

TODO: A vuex FSM to monitor the state of the application

*/