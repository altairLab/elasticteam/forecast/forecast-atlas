const Action = Object.freeze({
    UNKNOWN: -1,
    STANDBY:   0,
    LOGGING:  1,
    ERROR: 2,
    REQ_PARAM: 3
});

const AppMode = Object.freeze({
    LOW:   0,
    MEDIUM:  1,
    HIGH: 2
});

const ErrorType = Object.freeze({
    OTHER: -1,
    CHECKSUM: 0,
    USER: 1,
    MSG: 2
})

export {
    Action,
    AppMode,
    ErrorType
}
