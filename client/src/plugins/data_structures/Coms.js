class Parameter {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}

class Error {
    constructor(type, message = "") {
        this.type = type;
        this.message = message;
        this.timestamp = Date.now();
    }
}

export {
    Parameter,
    Error
}
