import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      dark: {
        primary: '#bb86fc',
        accent: '#bb86fc',
        secondary: '#03dac6',
        success: '#43AA8B',
        info: '#5d6a89',
        warning: '#F9844A',
        error: '#F94144',
        background: '#121212'
      },
      light: {
        primary: '#277DA1',
        accent: '#277DA1',
        secondary: '#4D908E',
        success: '#43AA8B',
        info: '#5d6a89',
        warning: '#F9844A',
        error: '#F94144',
        background: "#eee"
      }
    },
  },
});
