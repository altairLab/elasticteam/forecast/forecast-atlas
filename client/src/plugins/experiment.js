import { EventBus } from "@/plugins/bus.js";
import axios from "./axios"
import store from "./store"

class Experiment {
    constructor() {
        EventBus.$on("timer_finished", function () {
            this.stop_experiment().then(function () {
                let current_index = store.getters["status/experiment_current_loop"];
                let loops = store.getters["status/experiment_loops"];
                if (current_index < loops)
                    this.start_experiment();
                else {
                    EventBus.$emit("invoke_manualDisconnect");
                }
            }.bind(this));
        }.bind(this));

        EventBus.$on("logStarted", function () {
            store.dispatch("status/setRunning", true);
        });
    }

    init(n_loops) {
        store.dispatch("status/initExperiment", n_loops);
    }

    async start() {

        let message_type;
        let message;

        await axios
            .post("/startconnection")
            .then((res) => {
                store.dispatch("status/setConnected", res.data.success);
                message_type = "success";
                message = "Connection established";
            })
            .catch(() => {
                message_type = "error";
                message = "Can't connect to the board";
            });
        return { message: message, type: message_type };
    }

    async stop() {

        EventBus.$emit("disconnecting");

        let message_type;
        let message;
        await axios
            .post("/stopconnection")
            .then((res) => {
                if (store.getters["status/isRunning"])
                    store.dispatch("status/updateExperiment");
                store.dispatch("status/setRunning", !res.data.success);
                store.dispatch("status/setConnected", !res.data.success);
                store.dispatch("status/setLastRun", res.data.last_run);
                message_type = "success";
                message = "Board disconnected";
                EventBus.$emit("disconnected");
            })
            .catch(() => {
                message_type = "error";
                message = "Can't disconnect from the board";
            });
        return { message: message, type: message_type };
    }

    start_experiment() {

        store.dispatch("coms/reset");
        store.dispatch("logs/reset");
        EventBus.$emit("resetWidgets");

        let current_index = store.getters["status/experiment_current_loop"];
        axios
            .post("/startexperiment", { index: current_index })
            .then(() => {
            })
            .catch(() => {
            })
            .finally(() => {
            });
    }

    async stop_experiment() {
        store.dispatch("status/setRunning", false);
        await axios
            .post("/stopexperiment")
            .then(() => {
                store.dispatch("status/updateExperiment");
                store.dispatch("status/setRunning", false);
            })
            .catch(() => {
                store.dispatch("status/setRunning", true);
            })
            .finally(() => {
            });
    }

    on_timer_finish() {

    }
}

export let experiment = new Experiment();