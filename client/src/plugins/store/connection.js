
export default {
  namespaced: true,
  state: {
    serial: {
      baudrate: 921600
    }
  },
  mutations: {
    update_serial(state, payload) {
      for (let key in payload)
        state.serial[key] = payload[key];
    },
  },
  actions: {
    updateSerial({ commit }, payload) {
      commit("update_serial", payload);
    },

  },
  getters: {
    serial: (state) => state.serial,
  }
}