export default {
    namespaced: true,
    state: {
        parameters: [],
        errors: [],
        messages: []
    },
    mutations: {
        add_parameter(state, parameter) {
            state.parameters.push(parameter);
        },
        push_error(state, error) {
            state.errors.push(error);
        },
        push_message(state, message) {
            state.messages.push(message);
        },
        reset(state) {
            state.parameters = [];
            state.errors = [];
            state.messages = [];
        }
    },
    actions: {
        addParameter({commit}, parameter) {
            commit("add_parameter", parameter);
        },
        pushError({commit}, error) {
            commit("push_error", error);
        },
        pushMessage({commit}, message) {
            commit("push_message", message);
        },
        reset({commit}) {
            commit("reset");
        }
    },
    getters: {
        parameters: (state) => state.parameters,
        errors: (state) => state.errors,
        messages: (state) => state.messages
    }
}