
export default {

    namespaced: true,

    state: {
        log: [],        // [ Number, Number, Number, ... ]
        datasets: []    // [ {label: "nome", color: "#123456"}, ... ]
    },

    mutations: {
        display_log(state, log) {
            state.log = log;
        },
        addDataSet(state) {
            let colors = ["#ca6037",
                "#717ccd",
                "#75ab3d",
                "#bd5db0",
                "#54a77b",
                "#cc566c",
                "#b49242"];
            state.datasets.push({ label: "P" + eval(state.datasets.length + 1), color: colors[state.datasets.length] });
        },
        deleteDataset(state, index) {
            if (state.datasets.length <= 0)
                return;
            state.datasets.splice(index, 1);
        },
        updateLabel(state, { label, index }) {
            state.datasets[index].label = label;
        },
        updateColor(state, { color, index }) {
            state.datasets[index].color = color;
        },
        reset(state) {
            state.log = [];
            state.datasets = [];
        }
    },

    actions: {
        addDataset({ commit }) {
            commit("addDataSet");
        },
        updateDataset({ commit }, payload) {
            if ("color" in payload)
                commit("updateColor", payload);
            else
                commit("updateLabel", payload);
        },
        deleteDataset({ commit }, index) {
            commit("deleteDataset", index);
        },
        displayLog({ commit }, log) {
            commit("display_log", log);
        },
        reset({ commit }) {
            commit("reset");
        }
    },

    getters: {
        last_log: (state) => state.log,
        colors: (state) => state.datasets.map(x => x.color),
        labels: (state) => state.datasets.map(x => x.label),
        datasets: (state) => state.datasets
    }

}