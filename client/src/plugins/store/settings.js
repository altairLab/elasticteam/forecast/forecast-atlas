import { AppMode } from "@/plugins/data_structures/Enums";

export default {
    namespaced: true,
    state: {
        mode: AppMode.MEDIUM,
        chart: true,
        infinite_buffer: false,
        settings: {
            general: {
                mode: AppMode.MEDIUM
            },
            timer: {
                timer: true,
                timer_duration: 30,
                timer_disconnect: true
            }
        }
    },
    mutations: {
        set_mode(state, payload) {
            state.mode = payload.mode;
            state.chart = payload.chart;
            state.infinite_buffer = payload.infinite_buffer;
        },
        update_timer(state, payload) {
            for (let key in payload)
                state.settings.timer[key] = payload[key];
        }
    },
    actions: {
        setMode({commit}, value) {
            let payload = {
                mode: value,
                chart: false,
                infinite_buffer: false
            };
            switch(value) {
                case AppMode.LOW:
                    payload.chart = false;
                    payload.infinite_buffer = false;
                    break;
                case AppMode.MEDIUM:
                    payload.chart = true;
                    payload.infinite_buffer = false;
                    break;
                case AppMode.HIGH:
                    payload.chart = true;
                    payload.infinite_buffer = true;
            }
            commit("set_mode", payload);
        },
        updateTimer({commit}, payload) {
            commit("update_timer", payload);
        }
    },
    getters: {
        mode: (state) => state.mode,
        chart: (state) => state.chart,
        infinite_buffer: (state) => state.infinite_buffer,
        settings: (state) => state.settings,
        timer: (state) => state.settings.timer
    }
}