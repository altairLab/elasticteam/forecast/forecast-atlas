
export default {
    namespaced: true,
    state: {
        settings: false,
        about: false,
        errors: false
    },
    mutations: {
        settings(state, value) {
            state.settings = value;
        },
        about(state, value) {
            state.about = value;
        },
        errors(state, value) {
            state.errors = value;
        }
    },
    actions: {
        showAbout({commit}, value) {
            commit("about", value);
        },
        showSettings({commit}, value) {
            commit("settings", value);
        },
        showErrors({commit}, value) {
            commit("errors", value);
        }
    },
    getters: {
        settings: (state) => {
            return state.settings;
        },
        about: (state) => {
            return state.about;
        },
        errors: (state) => state.errors
    }
}