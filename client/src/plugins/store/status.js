export default {
    namespaced: true,
    state: {
        connected: false,
        is_running: false,
        last_run: {},
        experiment: {
            loops: 0,
            loops_done: 0
        },
        logssec: 0,
    },
    mutations: {
        initialize_experiment(state, value) {
            state.experiment.loops = value;
            state.experiment.loops_done = 0;
        },
        update_experiment(state) {
            state.experiment.loops_done += 1;
        },
        set_running(state, value) {
            state.is_running = value;
        },
        setConnected(state, value) {
            state.connected = value;
        },
        serialStats(state, values) {
            state.logssec = values.logssec;
        },
        last_run(state, value) {
            state.last_run = value;
        }
    },
    actions: {
        initExperiment({commit}, value) {
            commit("initialize_experiment", value);
        },
        updateExperiment({commit}) {
            commit("update_experiment");
        },
        setRunning({commit}, value) {
            commit("set_running", value);
        },
        setConnected({commit}, value) {
            commit("setConnected", value);
        },
        setSerialStats({commit}, value) {
            commit("serialStats", value);
        },
        setLastRun({commit}, value) {
            commit("last_run", value);
        }
    },
    getters: {
        isConnected: (state) => state.connected,
        isRunning: (state) => state.is_running,
        logsSec: (state) => state.logssec,
        last_run: (state) => state.last_run,
        experiment_loops: (state) => state.experiment.loops,
        experiment_current_loop: (state) => state.experiment.loops_done
    }
}