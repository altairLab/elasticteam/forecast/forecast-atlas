.PHONY = help install build run clean frontend
.DEFAULT_GOAL = help

help:
	@echo "---------------HELP-----------------"
	@echo "install - Install node dependencies"
	@echo "build - Build the frontend"
	@echo "frontend - Serve the frontend"
	@echo "run - Build & start the application"
	@echo "------------------------------------"

install:
	cd client && npm install

build: install
	cd client && npm run build

run: build
	forecastui

frontend:
	cd client && npm run serve
